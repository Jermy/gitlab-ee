# Web IDE

> Introduced in [GitLab Enterprise Edition Ultimate][ee] 10.4.

> **Warning:** Web IDE is in **Beta**. For the latest updates, check the
> associated [epic](https://gitlab.com/groups/gitlab-org/-/epics/26).

The Web IDE editor makes it faster and easier to contribute changes to your
projects by providing an advanced editor with commit staging.

## Enable the Web IDE

While in the early stages of the Beta, access to the Web IDE is by opting in.

To enable the Web IDE, click on your profile image in the top right corner and
navigate to **Settings > Preferences**, check **Enable Web IDE** and save.

![Enable Web IDE](img/enable_web_ide.png)

## Open the Web IDE

Once enabled, the Web IDE can be opened when viewing a file, from the
repository file list.

![Open Web IDE](img/open_web_ide.png)

## Commit changes

Changed files are shown on the right in the commit panel. All changes are
automatically staged. To commit your changes, add a commit message and click
the 'Commit Button'.

![Commit changes](img/commit_changes.png)

[ee]: https://about.gitlab.com/gitlab-ee/
